Welcome to pyMSDtorch's documentation!
======================================

pyMSDtorch provides easy access to a number of segmentation and denoising methods using convolution neural networks.
The tools available are build for microscopy and synchrotron-imaging/scattering data in mind, but can be used elsewhere
as well.

The easiest way to start playing with the code is to install pyMSDtorch and perform denoising/segmenting using custom
neural networks in our tutorial notebooks located in the pyMSDtorch/tutorials folder.

Check the readthedocs page or the README.md file for more information.