=======
History
=======

0.1.0 (2021-08-10)
------------------

* First release.


0.1.1 (2022-11-28)
------------------

* Second release
* getting ready for 'pip install pyMSDtorch'
* added docstrings as much as we can
* building template notebooks

0.1.2-6 (2022-12-15)
--------------------

* Second release - but now for real
* Ready for 'pip install pyMSDtorch'
* Documentation update
* Added notebooks and functionality for image classification

0.2.0 (2023-01-26)
--------------------

* Bug fixes, name changes and improvements.
* Added UNet3+
* Updated how to save and read in networks
* This will be last release under the pyMSDtorch name.
  Over the past years, we have included a lot more than an MSD
  architecture, and changing the name to fit is purpose makes more
  sense at this point. We will continue under the new name
  dlsia : Deep Learning for Image Analysis

