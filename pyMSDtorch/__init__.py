"""Top-level package for pyMSDtorch."""

__author__ = """Petrus Hendrik Zwart & Eric J. Roberts"""
__email__ = 'PHZwart@lbl.gov ; EJRoberts@lbl.gov'
__version__ = '0.2.0'
